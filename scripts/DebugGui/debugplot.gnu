set xrange [-10:10]
set yrange [-10:10]
set size 1, 1
set grid

plot "< awk '{if($3 == \"red\") print}' /home/ruud/workspace/Multilateration/Debug/mlat.out" using 1:2 t "red" pt 7 ps 1, \
     "< awk '{if($3 == \"green\") print}' /home/ruud/workspace/Multilateration/Debug/mlat.out" using 1:2 t "green" w p pt 2
reread
