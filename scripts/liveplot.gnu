set xrange [0:18]
set yrange [0:13]
set size 1, 1
set grid
plot "plot.dat" using 1:2 pt 7 ps 1

plot "< awk '{if($3 == \"red\") print}' Data.csv" u 1:2 t "red" w p pt 2, \
     "< awk '{if($3 == \"green\") print}' Data.csv" u 1:2 t "green" w p pt 2
reread
