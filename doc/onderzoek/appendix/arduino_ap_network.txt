
config interface 'loopback'
	option ifname 'lo'
	option proto 'static'
	option ipaddr '127.0.0.1'
	option netmask '255.0.0.0'

config interface 'lan'
	option proto 'static'
	option ipaddr '192.168.240.1'
	option netmask '255.255.255.0'

config interface 'wan'
	option ifname 'eth0'
	option _orig_ifname 'eth0'
	option _orig_bridge 'false'
	option proto 'static'
	option ipaddr '192.168.0.1'
	option netmask '255.255.255.0'
	option gateway '192.168.0.1'
	option broadcast '192.168.0.254'
	option dns '192.168.0.1'

config interface 'wan1'
	option ifname 'eth1'
	option proto 'dhcp'
	option metric '10'

