
Opdracht

De opdracht is om multilateration te realiseren met behulp van geluid signalen. Het doel is om
inzichtelijk te krijgen welke randvoorwaarden er zijn om een redelijk tot goede positiebepaling
te doen op basis van multilateration. Multilateration is een surveillance techniek gebaseerd op
het meten van het verschil in afstand door middel van twee of meer stations op bekende locaties.
De ultieme wens is te komen tot een opstelling waarmee in een lokaal de positie kan worden
bepaald van een geluidsbron (spreker). Omdat het geluidsignaal relatief traag is, in vergelijking
met radiosignalen in het luchtverkeer, is het de hoop dat de invloed van tijd-meetfouten kan worden
beperkt en kan worden onderzocht.
Tijdens dit project wordt er onderzoek verricht naar hoe het mogelijk is om multilateration te
realiseren op 2D niveau. Wanneer dit duidelijk is, wordt er een proof of concept gemaakt waar
multilateration wordt toegepast. Dit proof of concept zal door middel van geluid en microfoons
laten zien dat de formules en het algoritme uit het onderzoek toepasbaar zijn. De onderzoeksvraag
die bij dit project hoort luid: Is de locatie van een geluidsbron op 1 meter nauwkeurig te benaderen
door middel van multilateration multilateration te realiseren in een 2D omgeving?
Er zal gedurende dit project niet meer dan een proof of concept gemaakt worden. Het bepalen van
locaties van vliegtuigen behoort niet binnen het project.

7

4

Aanpak

4.1

Algemeen

Voor ieder project is projectplan nodig. Deze is gemaakt in de eerste weken van de stage periode.
Omdat de verwachting was dat een groot deel van het project uit onderzoek zou bestaan, neemt
deze fase ongeveer 35% van de projecttijd in. Dit onderzoek is op te verdelen in 2 grotere stukken.
Een deel van het onderzoek gaat over de hardware die gebruikt zal worden in de toepassen en het
tweede deel van het onderzoek is een wiskundig onderzoek naar het multilateration algoritme.
Voordat het onderzoek van start kan gaan is in samenspraak met de opdrachtgever ”user requirements” op gesteld welke vertaald worden in ”system requirements”. Voor dit proces staat 2
weken gepland om zo genoeg tijd te hebben voor reviews van andere partijen. Door middel van de
requirements op te stellen is het mogelijk om specifieke onderzoeksvragen te formuleren.
Mijn wens betreft dit project is om een toepassing van het gevonden resultaat te laten zien.
Hiervoor is het nodig om een applicatie te implementeren. Voor het implementeren heb ik tijd
ingepland om te kunnen denken over hoe de applicatie gemplementeerd zal worden. Ik weet dat
mijn sterkste punt niet bij het design ligt. Hierdoor heb ik genoeg tijd ingeplant om het design te
kunnen reviewen, maar relatief weinig tijd omdat er dan niet veel kostbare tijd verloren gaat met
het perfectioneren. De verwachting is echter dat het design tijdens het implementeren aangepast
zal worden.
In de implementatie fase zullen er stukken van de onderzoeksfase in een geheel gezet worden. Hierdoor wordt tijd gewonnen om kleine stukken software uit de tests van het onderzoek te gebruiken
in de applicatie. Omdat de applicatie veel hardware bevat en zonder de hardware de applicatie
weinig tot niets kan, is ervoor gekozen om de testfase vroeger te laten beginnen dan het einde van
de implementatie fase. Veel dingen kunnen alleen getest worden als de meeste functies werken. De
fouten of scenario’s waar niet op gerekend is zullen pas aan het licht komen tijdens het testen.

4.2
4.2.1

Tools
LaTeX

In het kader van ”Methodish en Gestructureerd” is er gekozen om alle documentatie in LaTeX te
maken. Deze keuze is gebaseerd op de volgende dingen:
• Handige toepassing van hetzelfde format.
• Door ”plain text files” gemakkelijke toepassing van versie beheer.
• Eerder gebruikt door de uitvoerende
• In gebruik door het stage bedrijf
• Verwachting van complexe formules te moeten documenteren, waar LaTeX min of meer voor
gemaakt is.
• Externe documenten worden ”geı̈nclude” waardoor aanpassingen meteen in de documentatie
worden meegenomen.
• Resulteert in een PDF wat voor elk systeem met verschillende ”operating systems” te lezen
is.

8

• Een document voor ieder hoofdstuk waardoor structuur ontstaat.
Elk document heeft de volgende structuur in een file browser: In dit voorbeeld heet het document
”TestDocument”.
doc
d: TestDocument
d: images
f: image1.png
d: appendix
d: bib
f: TestDocument.bib
d: tex
f: hoofdstuk1.tex
f: hoofdstuk2.tex
f: appendix.tex
f: TestDocument.tex
f: TestDocument.pdf
In TestDocument.tex worden de files in de directory tex en in bib ”geı̈nclude”. In appendix.tex
worden de files in de directory appendix ”geı̈nclude”. De PDF wordt automatisch gegenereerd
door het document te ”compilen”. Tijdens het ”compilen” van het document ontstaan er meerdere
bestanden die door het versiebeheer programma worden genegeerd.
4.2.2

Linux

Tijdens dit project wordt voornamelijk gebruik gemaakt van Linux distributies. Deze keuze lag
voor de hand aangezien het stage bedrijf hun software ontwikkelt op en voor linux systemen.
Daarnaast werkt de stage uitvoerder voor 90% van de tijd op Unix based systemen.
4.2.3

Eclipse

Het stage bedrijf ontwikkeld hun software in C++ met als IDE Eclipse. Tijdens het ontwikkelen
van software voor de stage applicatie zal dit ook gebruikt worden. Als er problemen te voorschijn
komen betreft IDE is er binnen het bedrijf genoeg kennis om weer senl opgang te komen.
4.2.4

Git

Git is een gratis en open source versie beheer systeem het is gemakkelijk te leren en is tegenover svn
zeer snel.[2] In voorgaande projecten heb ik gemerkt hoe vervelend het kan zijn als data verloren

9

gaat. Met Git kun je de data altijd opslaan in een ”remote reposetory”. Naast dit voordeel is het
mogelijk om veranderingen bij te houden en eventueel terug gaan naar een eerdere versie mocht
dit nodig zijn.
Git heeft ook opties om ”branches” te maken zodat het gemakkelijk is om met meerdere mensen
aan hetzelfde project te werken. Aangezien ik dit project alleen doe, zal deze optie niet gebruikt
worden.

10

5

Onderzoek

In bijlage ?? is het betreffende onderzoeksdocument te vinden.
Zoals vermeld in hoofdstuk 4 is het onderzoek te verdelen in 2 stukken; hardware en algoritme.
Aangezien er levertijd op hardware zit, is er voor gekozen om de onderzoeksfase te beginnen met
het onderzoek hardware gedeelte. Wanneer een conclusie getrokken is zal de hardware besteld
worden. Tijdens de leverperiode wordt het algoritme onderzocht. Wanneer de hardware binnen
is, wordt weer verdergegaan met de hardware om te testen of de hypothesen kloppen. Mocht dit
niet het geval zijn, wordt er opnieuw hardware besteld en zal het onderzoek betreft het algoritme
hervat worden. Mocht de hardware voldoen aan de eisen en verwachtingen, zal het onderzoek
verder volledig gericht worden op het algoritme.

5.1

Hardware

Het eindproducten van dit project is een toepassing voor multilateration op 2d niveau door middel
van geluidsignalen. Om iets te kunnen doen met geluidsignalen, moet het signaal opgevangen
worden door meerdere sensoren. Deze sensoren zullen bestaan uit een microcontroller en een
microfoon. Of deze 2 componenten genoeg zullen zijn om de requirements te halen zal blijken uit
het onderzoek.
Tijdens het onderzoek van de hardware wordt gebruik gemaakt van de volgende strategieı̈n:
• Bieb
Bieb strategie wordt gebruikt om opzoek te gaan naar materiaal wat beschikbaar is. Er wordt
gekeken naar de specificaties van verschillende componenten om zo tot een goede keuze te
komen. Deze keuze zal vervolgens getest worden.
• Lab
Deze strategie is zeer belangrijk bij het onderzoek betreft hardware. De conclusies die bij de
bieb strategie tot stand gekomen zijn, worden door middel van deze strategie gevalideerd.
• Showroom
Na het tot stand zijn gekomen tot een oplossing, wil het niet altijd zo zijn dat de oplossing
de beste aanpak is. Na het vinden van een oplossing wordt gekeken hoe anderen een zelfde
of veel overeenkomend probleem hebben opgelost. Dit resulteert in betere en meer robuuste
oplossingen.
De hoofdvraag die bij dit deel van het onderzoek hoort is:
Welke hardware is nodig om multilateration te realiseren op een nauwkeurigheid
van 1 meter door middel van geluid?
Er is begonnen om te achterhalen welke componenten nodig zijn voor dit gedeelte van de applicatie.
Samen met de opdrachtgever is gebrainstormd over mogelijkheden voor de hardware. Enkele opties
zijn:
• 1 device met meerdere microfoontjes die vrijwel alles doet.
• 1 server die multilateration afhandelt, 1 device die de waardes van de microfoontjes verwerkt
• Voor ieder microfoontje een device die zijn microfoon afhandelt en verstuurd naar een server.

11

Aangezien ”single point of failure” geen goed idee is, is gekozen voor de laatste optie. Door aparte
devices te maken zijn de sensoren meer mobiel aangezien de mogelijkheden oneindig groot worden
mochten de sensoren op het internet aangesloten worden. Dit concept is te zien in figuur 1. Door
deze aanpak te kiezen za de sensor uit een microcontroller en een microfoon gaan bestaan.

Sensor 1

Sensor 2

Server

Sensor 3

Sensor 4

Figuur 1: Schematisch overzicht hardware opstelling

5.1.1

Microcontroller

Aan het begin van het onderzoek van de microcontrollers is gekeken naar welke microcontrollers
/ microcomputers er beschikbaar zijn die met een microfoon overweg kunnen. Het aanbod is zeer
veel. Om deze reden is gekozen het onderzoek bij de controllers te beperken die bekend zijn bij
de uitvoerende. Het onderzoek zal vanaf nu beperkt blijven bij een Arduino. De volgende vragen
zullen beantwoord worden:
• Hoe snel moet de controller zijn om de locatie van een geluidbron te bepalen op 1 meter
nauwkeurig?
• Welke aansluiting heeft de microcontroller nodig om te communiceren met de server?
Dankzij de informatie op sparkfun.com is makkelijk te zien hoe snel verschillende Arduino’s zijn.[1]
Na een test te hebben gedaan met een al beschikbare Arduino Uno, is berekent dat deze een Arduino minimaal een snelheid van 557 KHz moet hebben om op een meter nauwkeurig de positie
te kunnen bepalen. Alle beschikbare Arduino’s zitten in de Mhz range. De conclusie is dat een
Arduino snel genoeg is voor de betreffende applicatie.
Voor de communicatie met een Arduino zijn verschillende opties mogelijk. Omdat er geen specificaties zijn betreft de communicatie, is gekozen voor een veelgebruikte communicatie middel in
grote en kleine netwerken namelijk ”internet”. In combinatie met de Arduino zijn de volgende
dingen mogelijk:

12

Naam
Ethernet Shield
Wifi Shield
Yun Shield
Arduino Ethernet
Arduino Uno Wifi
Arduino Yun

Ethernet
x
x
x
x

Wifi
x
x
x
x

Snelheid (MHz)
44.1
44.1
44.1

Prijs
10,40,40,50,40,70,-

Na de voordelen en nadelen op een rijtje te hebben gezet, is in overleg met de opdrachtgever
gekozen om Arduino Yun’s aan te schaffen. Deze keuze is gebaseerd op de volgende punten:
• De Yun bevat een mogelijkheid voor ethernet en wifi.
• De Yun heeft 2 processoren waarvan 1 een AVR chip is die aangesloten zit op de IO poorten,
de andere processor bevat een Linux distributie.
• Ze zitten ruim binnen het budget.
• Ze zijn inzetbaar voor veel verschillende toepassingen.
• Door de Linux distributie is het gemakkelijk een eigen wifi netwerk op te zetten met een
”acces point”.
• door de twee processoren gaat er geen tijd verloren aan communicatie door de AVR chip
aangezien de Linux distributie dit afhandelt
Deze voordelen bevat het ”Yun shield” ook. Het nadeel van het yun shield is dat er redelijk wat
IO pinnen wegvallen die gebruikt worden door het shield. Aangezien er Arduino Uno’s aangeschaft
moeten worden om het shield te gebruiken, liggen de kosten zeer dicht bij elkaar.
Na een tijdje te hebben gezocht naar de Arduino Yun om deze te kunnen bestellen, is de conclusie
dat dit bordje niet meer gemaakt wordt. Dit vanwege redenen betreft rechten op de naam. In de
plaats van de Arduino Yun is de Seeeduino Cloud gekomen.[3] De Seeeduino Cloud is echter wel
te bestellen en werkt precies hetlzefde als de Yun. De IDE, libraries en compilers kunnen gebruikt
worden voor de Seeeduino Cloud. Er zijn 2 Seeeduino’s besteld om te testen of de verwachtingen
van het bordje klopt.
Aangezien het bordje een OpenWrt distro heeft runnen is het plan om 1 bordje te laten dienen als
een acces point voor WIFI waar dan de andere bordjes met kunnen verbinden. Dit reduceert de
kosten aangezien routers, accespoints en switches niet nodig zijn voor communicatie. Tijdens het
wachten op de levering is begonnen aan het onderzoek over het multilateration algoritme.
Na het binnen krijgen van de 2 Seeeduino’s, ben ik begonnen om te testen of ik werkelijk een
Wifi netwerk kan opzetten waarbij 1 Seeduino een Accespoint is. Zoals eerder genoemd werk ik
voor 90% met Linux en staat er een Linux distributie op de Seeeduino’s. Wat mij opviel is dat de
configuratie bestanden van de Linux distributie niet op de gebruikelijke plek staan. Een voorbeeld
hiervan is dat de betreffende distributie ”dnsmasq” gebruikt. Het configuratie bestand van dit programma is normaal /etc/dnsmasq.conf. In de Linux distrubutie staat het betreffende document
in /etc/config/ en heeft het een andere syntax dan het gewoonlijke dnsmasq configuratie bestand.
Na eenmaal deze constructie door te hebben was de configuratie zo gedaan.
Omdat de Seeeuino 2 processoren heeft verwacht ik dat het hier moeilijk wordt betreft communicatie. Na onderzoek te hebben verricht op verschillende fora en arduino.cc, heb ik een chat client

13

en server applicatie weten te maken tussen de Seeeduino’s door middel van een bridge library. De
data stroom gaat nu als volgt:
Atmega32U 4 > Draginomodule > W if imodule > W if imodule > Draginomodule > Atmega32u4
Waarbij de Atmega C++ code draait en de dragino module een Linux distributie. In bijlage ??
staat het proces uitgebreider beschreven en is de source code te vinden van de chat applicatie.
Op dit punt is het nog niet duidelijk welke microfoon gebruikt zal worden. Om snelheid te waarborgen tijdens het meten van geluid, zal er een interrupt getriggerd worden wanneer geluid gedetecteerd
wordt. Ik weet dat alle Arduino’s in staat zijn om een digitale interrupt te triggeren. Nu weet ik
ook dat een microfoon geen digitaal signaal zal geven. Ik ging onderzoeken of het mogelijk is om
met een analoog signaal een interrupt te triggeren.
Na het hebben bekeken van fora wist ik in welk hoofdstuk van de datasheet van de Atmega32U4
ik moest gaan kijken. Na een test opstelling gemaakt te hebben met een potmeter en verschillende
pogingen van het veranderen van de juiste bitjes in de registers, ben ik tot een succesvol programma
gekomen wat een analoog interupt triggert. Dit houd in dat een interrupt getriggerd wordt bij de
opgaande flank van een analoog signaal. in bijlage ?? staat het betreffende in detail uitgelegd.
De bovenstaande resultaten van de testen zijn naar verwachting. De conclusie die hieruit te trekken valt is dat de Seeeduino Cloud een geschikte microcontroller is voor de toepassing. Nadat de
resultaten zijn besproken met de opdrachtgever, zijn er 3 extra Seeeduino Clouds besteld zodat
het totaal op 5 komt. De verwachting op dit moment is dat er minimaal 3 sensoren nodig zullen
zijn. Door 5 te kunnen gebruiken is het in de toekomst mogelijk om te testen wat de invloed is
van meerdere sensoren.
5.1.2

Microfoon

Om geluid te kunnen detecteren is een microfoon nodig. Het idee is om een interrupt te triggeren
wanneer de sensor een opgaande flank in geluid detecteert. In figuur 2 is te zien hoe geluid eruit
ziet en wat precies bedoeld wordt met de opgaande flank. De microcontroller moet dus geactiveerd
worden wanneer het geluid boven waarde B komt.

14

Figuur 2: Geluids golf

Tijdens het bestellen van de microcontrollers zijn er 2 verschillende microfoontjes aan de bestellijst toegevoegd om deze te testen. De betreffende microfoontjes zijn:
• Silicon MEMS microphone Breakout
• I2S microphone
Ik ben begonnen met het aansluiten van de Silicon MEMS microphone Breakout aan een arduino
om vervolgens de inkomende waardes te lezen. Uit deze data kon ik geen bruikbare conclusies
trekken. Op de stage locatie is echter wel een osciloscoop aanwezig waar ik gebruik van kan
maken. Het signaal van het microfoontje wat de voeding krijgt van een arduino kan nu gemakkelijk
gevisualiseerd worden. In bijlage ?? is dit proces in detail beschreven. De conclusie van de test is
dat de microfoon een lage gevoeligheid heeft. De arduino is niet in staat om het verschil te kunnen
meten tussen waarde B en waarde A (zie figuur 2). Er zal gebruik gemaakt moeten worden van
een signaal versterker wil dit microfoontje bruikbaar zijn voor de toepassing.
Na de test te hebben gedaan met het eerste microfoon is het nu tijd om de volgende te testen. De
I2S microphoon is een digitaal microfoon die werkt met het I2S protocol. Aangezien er al redelijk
wat code nodig is om dit protocol te laten werken, is het niet mogelijk een interrupt te triggeren
bij een specifieke waarde. Naast dit probleem wordt in bijlage ?? beschreven dat de processor
snel moet zijn om een redelijke kwaliteit audio te kunnen detecteren. De processor die aanwezig
is op de Seeeduino cloud is hiervoor niet geschikt. Volgens berekeningen zou de processor wel snel
genoeg zijn om het protocol te implementeren. Er blijft echter niet veel ruimte over voor error en
al helemaal niet om iets te kunnen doen met het gelezen geluid. Dit maakt een implementatie van
I2S voor de betreffende processor nutteloos. De conclusie is dat deze microfoon niet gaat werken
binnen de betreffende applicatie.
Na deze tegenvallers weet ik beter waar ik naar moet kijken om een goede microfoon te zoeken. Ik
ben verder gaan kijken en ben de volgende opties nagegaan:
15

• Aanwezige microfoon met opamp.
• Aanwezige microfoon met flip flop schakeling.
• microfoon met opamp in een.
• microfoon met flip flop in een.
Aangezien het onderzoek van de microcontroller op dit punt al afgerond is, weet ik dat de controller
instaat is om een digitale interrupt en een analoog interrupt te kunnen trigeren. Door deze informatie maakt het niet veel uit of de microcontroller een analoog of digitaal signaal krijgt. Tijdens
de testen met de oscilloscoop met de eerste microfoon, was redelijk wat ruis te zien in het signaal
van de voeding van de Arduino. Om geen last te hebben van ruis, is gekozen om alle hardware
op 1 pcb te houden zodat signalen geen relatief lange afstanden hoeven af te leggen. Vervolgens is
gekozen voor de flip flop schakeling aangezien een opamp zonder input al random voltages uitstuurt
(ruis). Met een digitaal signaal zijn de random voltages verwaarloosbaar en heeft dit geen invloed
op data.
De Xinda KY-038 is een microfoon die een digitaal signaal uitstuurt wanneer het signaal boven
een treshold uitkomt die ingesteld is door de aanwezige potmeter. Zoals te zien is in figuur 3 zit
alles op 1 pcb.
In bijlage ?? staat vermeld dat met deze microfoon een test is uitgevoerd om een digitale interrupt
te triggeren met een Seeeduino. De test is succesvol waardoor de Xinda KY-038 geschikt is voor
de toepassing.

Figuur 3: Xinda KY-038

5.2

Algoritme

De verwachting van dit stukje onderzoek is dat het pittige wiskunde zal zijn. Naar mijn mening is
het van belang dat ik de wiskunde achter het algoritme begrijp om tot een bruikbare toepassing
te komen. Ik heb gekozen om dit onderzoek vanaf het begin af aan te beginnen. Ik ben begonnen
met het uitleggen waarom het niet mogelijk is om multilateration toe te passen met 1 sensor. Het
is vrij logisch waarom dit niet gaat werken, maar het is een mooie opbouw naar het antwoord
waarom er meerdere sensoren nodig zijn voor er een oplossing komt.
Vervolgens ben ik gaan kijken naar de situatie met 2 sensoren. Om duidelijkheid te verschaffen heb
ik situaties uitgetekend en plaatjes toegevoegd. Mijn verwachting bij deze situatie is dat er geen
precieze oplossing uit komt, maar een hyperbool met oneindig aantal punten. Door op formules
te creren en er vervolgens een voorbeeld bij te doen, ben ik in staat om te laten zien dat er een
16

hyperbool uitkomt. De informatie die uit de situatie met 2 sensoren is ontstaan, is bruikbaar voor
de komende situaties.
Met 3 sensoren had ik verwacht om tot een formule te komen die 1 punt kan uitrekenen. Aangezien
er meetfouten in de applicatie zitten, is het niet mogelijk om met een stel vergelijkingen op een
precies punt te komen. De oplossing van dit probleem is de ”least square solution”. Door middel
van matrixen te gebruiken kan de ”least square solution” het ”beste” punt berekenen. Onderzoek
naar hoe dit werkt is buiten het bereik van de opdracht.
Om een bruikbare vergelijking op te stellen zodat de matrix en vector met deze vergelijking gevuld
kan worden, zijn 3 sensoren nodig. In bijlage ?? is te zien hoe dit gedaan is.
In bijlage ?? is ook te zien dat voor iedere rij in de matrix een ander ”referentie sensor” is genomen
om de vergelijking te creëren. Echter geeft deze methode niet de juiste oplossing. De verschillen
in de getallen zijn zodanig klein, dat de oplossing nergens op slaat. Deze methode is dus niet
bruikbaar.
Door 4 sensoren te nemen en zo elke vergelijking op te stellen door middel van unieke sensor trio’s,
is het wel mogelijk om tot het juiste punt te komen. In bijlage ?? is een voorbeeld te zien dat de
methode tot een succes leid.
Door het nemen van meer dan 4 sensoren, is het mogelijk om meerdere unieke trio’s te vormen.
Er zijn testen gedaan met 50 en 1000 antennes. Bij beide testen kwam een oplossing. Echter nam
het algoritme zeer veel tijd in beslag bij de test met 1000 antennes. Het doel van de test is om te
kijken of de methode robuust is. Hierom zijn deze testen geslaagd. De uiteindelijke formule kan
meegenomen worden naar de volgende fase van het project.

17

6

Implementeren

-socat

18

7

Testen

7.1

Server

De applicatie is een groot geheel van 6 verschillende apparaten die met elkaar samen werken. Om
te voorkomen dat er fouten in de kleinere klassen zitten of ontstaan, heb ik een aantal simpele
unit tests geschreven om deze klassen te testen. Mocht ik ergens snel aanpassingen maken kan ik
gemakkelijk zien of ik niet iets cruciaal kapot heb gemaakt.
Omdat de server veel werkt met informatie van buitenaf, is het voor ingewikkeldere klassen vrijwel
onmogelijk om deze te testen d.m.v. unit tests. Hiervoor heb ik de berichten berekend van een
scenario zoals ze zouden moeten zijn. Dit scenario is te zien in bijlage ??. Met dit scenario is te
concluderen of de server in het geheel werkt of niet.

7.2

Sensor

Voor het testen van individuele sensoren is er een ”debug” klasse gemaakt die te activeren is met
een boolean variabele. Deze klasse laat zien waar de sensor is in de software. Hiermee kan getest
worden wanneer een interrupt geactiveerd wordt, of er een logische tijd gegeven wordt aan het
interrupt moment en of er werkelijk een connectie met de server is opgezet.

7.3

Geheel

Na de bovenstaande producten getest te hebben, kan het gehele systeem getest worden. De sensoren
worden hierbij in een ruimte verspreid. Met een rolmaat wordt opgemeten waar de sensoren zich
precies bevinden. Deze locaties worden ingevoerd in het configuratie bestand van de server.
De resultaten van de eerste test waren onverwachts. Het geheel functioneerde meteen zoals gewenst.
De volgende punten gingen meteen goed:
• De connectie tussen sensoren en server werden opgezet.
• De server ontving berichten van alle sensoren in de juiste formaat.
• Het ijk proces werd als eerste gestart en succesvol afgerond.
• De berichten werden gecheckt of ze binnen de 100 ms ontvangen waren.
• Het multilateration algoritme werd getriggerd.
• Er kwam een oplossing uit.
Echter was de oplossing niet de gewenste oplossing. De locatie die gemeten is heeft soms een
afwijking van 10 meter in een lokaal van 7 x 7 meter. Aangezien de server getest is met de
simulatie, is te concluderen dat de algoritmes op de juiste manier werken. De fout zit ergens
tussen het uitsturen van het signaal vanuit de signaal bron en het versturen van het bericht door
de sensor module.
Na het analyseren van de sensor software, is er geen duidelijke conclusie gekomen wat er fout is
aan het systeem. Als gevolg hiervan is er een software review gepland door de stage begeleider.
Na over het een en het ander gediscussieerd te hebben en aanpassingen te hebben gemaakt aan de
software, kan dezelfde test weer gedaan worden.
19

Tijdens deze test werden berichten meerdere keren verstuurd en leek de interrupt niet goed te
werken. Het is genoodzaakt om de software terug te zetten naar een eerdere versie. Met kleine
aanpassingen door te voeren die wel werkte, hadden de resultaten nog steeds een te grote afwijking.
Na deze resultaten verkregen te hebben, is er een test gedaan om te kunnen concluderen of de
interne klok van de Seeeduino’s gelijk lopen. Dit is gedaan door middel van de delta van de tijd
tussen 2 Seeeduino’s te meten. Hieruit is gebleken dat de interne klokken niet perfect gelijk lopen.
Gedurende deze test is er gebruik gemaakt van een interrupt waar de microfoon op reageert. Er
bestaat een mogelijkheid dat de interrupt het interne tel proces van microseconden onderbreekt
waardoor deze waardes niet overeenkomen op beide Seeeduino’s.

20

8

Conclusie en Aanbevelingen

Multilateration op 2D niveau met een Seeeduino is niet mogelijk op een meter nauwkeurig. Multilateration op 2D niveau met gesimuleerde hardware is wel mogelijk op een meter nauwkeurig. Ik
verwacht dat multilateration met hardware wel mogelijk is. Echter is er hiervoor meer onderzoek
nodig naar de interne klok van de Seeeduino. Mocht deze niet voldoen aan de eisen die nodig zijn,
is er onderzoek nodig naar andere hardware die wel aan de betreffende eisen voldoet. Een andere
aanbeveling is om te onderzoeken wat de invloed is van natuurkundige factoren zoals temperatuur
verschil en luchtdruk in de omgeving.
Betreft het ijk proces kan dit preciezer door middel van bijvoorbeeld gebruik te maken van een
geluidsbron waar de precieze locatie van bekend is en de tijd wanneer het signaal verstuurd is.
Hierdoor is er meer informatie om te berekenen wat de correctie voor iedere sensor moet zijn
Geurende het test proces is aan het licht gekomen dat de interrupt van de Atmega32u4 in combinatie met de Arduino bibliotheek niet geheel werkt zoals verwacht. Hierover kan verdere onderzoek
verricht worden om het geheel te eventueel te laten werken.
