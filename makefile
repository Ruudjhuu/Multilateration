.PHONY:doc all clean

all:

doc:
	$(MAKE) -C doc

clean:
	$(MAKE) -C doc clean