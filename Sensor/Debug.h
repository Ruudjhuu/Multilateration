#ifndef DEBUG_H_
#define DEBUG_H_

#include <Arduino.h>

class Debug {
  public:
    static void Begin(String endpoint);
    static void Error(String error_message);
    static void Warning(String warning_message);
    static void Notification(String notification_message);
    static void Notification(String notification_message, int number);
    
  private:
    Debug();
};

#endif /* DEBUG_H_ */
