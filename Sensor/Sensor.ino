//
//This program triggers an interrupt on mic_pin and
//sends a time stamp in microseconds via TCP to
//IP:PORT defined in Communication.h. The interrup
//debounce is defined by DEBOUNCE_TIME in micro seconds
//
#include "Communication.h"
#include "Debug.h"

#define DEBOUNCE_TIME_MS 1000  //ms
#define DEBOUNCE_TIME_US 1000*DEBOUNCE_TIME_MS  //us
#define IDLE_TIME_US 50       //us
#define DEBUG_ENDPOINT "console"
#define DEBUG false

int sensor_id = 0;
const int mic_pin = 7;
const int id_pin_1 = 8;
const int id_pin_2 = 9;
const int id_pin_3 = 10;
volatile unsigned long time = 0;
volatile bool interrupt = false;
Communication* communication;

void disarmInterrupt() {
  //cli();
  //detachInterrupt(digitalPinToInterrupt(mic_pin));
}

void handleInterrupt() {
static unsigned long last_interrupt_time = 0;
unsigned long interrupt_time= micros();
if (interrupt_time - last_interrupt_time > DEBOUNCE_TIME_US)
  {
  // silence the microphone
  disarmInterrupt();
  time=interrupt_time;
  interrupt=true;
}
last_interrupt_time=interrupt_time;
}

void armInterrupt() {
  interrupt=false;
  //sei();
}


void loadSensorId(){
  Debug::Notification("LoadSensorId...");
  pinMode (id_pin_1, INPUT) ;
  pinMode (id_pin_2, INPUT) ;
  pinMode (id_pin_3, INPUT) ;
  int pin_1 = digitalRead(id_pin_1);
  int pin_2 = digitalRead(id_pin_2);
  int pin_3 = digitalRead(id_pin_3);
  Debug::Notification("LoadSensorId: pin_1: ", pin_1);
  Debug::Notification("LoadSensorId: pin_2: ", pin_2);
  Debug::Notification("LoadSensorId: pin_3: ", pin_3);
  if(pin_1 > 0) {sensor_id += 1;}
  if(pin_2 > 0) {sensor_id += 2;}
  if(pin_3 > 0) {sensor_id += 4;}
  Debug::Notification("LoadSensorId: sensor_id: ", sensor_id);
}

void setup() {

  Bridge.begin();
  if (DEBUG) {
    Debug::Begin(DEBUG_ENDPOINT);
  }
  loadSensorId();
  Debug::Notification("Setup...");
  Debug::Notification("Create communication...");
  communication = new Communication(sensor_id);
  Debug::Notification("Create communication...  Done");
  Debug::Notification("Set pinmode...");
  pinMode (mic_pin, INPUT_PULLUP) ;
  Debug::Notification("Set pinmode...  Done");
  Debug::Notification("Attatch interrupt...");
  attachInterrupt(digitalPinToInterrupt(mic_pin),handleInterrupt,RISING);
  armInterrupt();
  Debug::Notification("Attatch interrupt...  Done");
  Debug::Notification("Setup...  Done");
}

void loop() {
  if (interrupt) {
    // the interrupt is disarmed now, we're not listening
    // send our timestamp...
    communication->SendTimeStamp(time);
    // wait a while before listening for the next sound
    //cli(); 
    delay(DEBOUNCE_TIME_MS); //sei();
    armInterrupt();
   }
   communication->Reconnect();
}

