#include "Debug.h"

#include <Bridge.h>

static String debug_endpoint;

void Debug::Begin(String endpoint) {
  debug_endpoint = endpoint;
  if(debug_endpoint == "console"){
    Console.begin();
    while(!Console){}
    Debug::Notification("Console debug started");
  }
  if(debug_endpoint == "serial"){
    Serial.begin(9600);
    while(!Serial){}
    Debug::Notification("Serial debug started");
  }
}
void Debug::Error(String error_message){
  if(debug_endpoint == "console"){
    Console.print("Error: ");
    Console.println(error_message);
  }
  if(debug_endpoint == "console"){
    Serial.print("Error: ");
    Serial.println(error_message);
  }
}

void Debug::Warning(String warning_message){
  if(debug_endpoint == "console"){
    Console.print("Warning: ");
    Console.println(warning_message);
  }
  if(debug_endpoint == "console"){
    Serial.print("Warning: ");
    Serial.println(warning_message);
  }
}

void Debug::Notification(String notification_message){
  if(debug_endpoint == "console"){
    Console.print("Notification: ");
    Console.println(notification_message);
  }
  if(debug_endpoint == "serial"){
    Serial.print("Notification: ");
    Serial.println(notification_message);
  }
}

void Debug::Notification(String notification_message, int number){
  if(debug_endpoint == "console"){
    Console.print("Notification: ");
    Console.print(notification_message);
    Console.println(number);
  }
  if(debug_endpoint == "serial"){
    Serial.print("Notification: ");
    Serial.print(notification_message);
    Serial.println(number);
  }
}
