#include "Communication.h"
#include "Debug.h"
//#include "TimeMessage.h"

Communication::Communication(int sensor_id) {
  Debug::Notification("Communication: Initalize...");
  this->m_sensor_id = sensor_id;
  this->m_connected = false;
  Debug::Notification("Communication: Initalize...  Done");
  this->Connect();
}

Communication::~Communication() {

}

void Communication::Connect() {
  Debug::Notification("Communication: Connecting...");
  if (this->IsConnected()) {
    Debug::Warning("Communication: Already Connected...");
    return;
  }
  this->m_client.connect(IP, PORT);
  if (this->IsConnected()) {
    Debug::Notification("Communication: Connecting...  Done");
    return;
  }
  Debug::Error("Communication: Connecting...   Failed");
}

void Communication::Disconnect() {
  Debug::Notification("Communication: Disconnecting...");
  if (!this->IsConnected()) {
    Debug::Warning("Communication: Already Disconnected...");
    return;
  }
  this->m_client.stop();
  Debug::Notification("Communication: Disconnecting...  Done");
}

void Communication::Reconnect() {
  if (!this->IsConnected()) {
    Debug::Notification("Communication: Reconnecting...");
    this->Connect();
    if (!this->IsConnected()) {
      Debug::Error("Communication: Reconnecting...  Failed");
    }
  }
}

void Communication::SendTimeStamp(unsigned long time) {
  if (this->IsConnected()) {
    Debug::Notification("Communication: SendTimeStamp...");
    String message = "%" + String(this->m_sensor_id) + " " + String(time) + "# ";
    this->m_client.print(message);
    Debug::Notification("Communication: SendTimeStamp...  Done");
  }
}

bool Communication::IsConnected() {
  return this->m_client.connected();
}

