#ifndef COMMUNICATION_H_
#define COMMUNICATION_H_

#include <Bridge.h>
#include <BridgeClient.h>

#define IP {192,168,200,100}
#define PORT 8080


class Communication {
  public:
    Communication(int sensor_id);
    virtual ~Communication();
    bool IsConnected();
    void SendTimeStamp(unsigned long message);
    
    void Reconnect();

  private:
    void Connect();
    void Disconnect();
    
    BridgeClient m_client;
    int m_sensor_id;
    bool m_connected;
};

#endif /* COMMUNICATION_H_ */

