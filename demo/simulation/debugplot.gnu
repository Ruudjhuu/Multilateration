set xrange [-1:21]
set yrange [-1:21]
set size 1, 1
set grid

plot "< awk '{if($3 == \"red\") print}' mlat.out" using 1:2 t "purple" pt 7 ps 1, \
     "< awk '{if($3 == \"green\") print}' mlat.out" using 1:2 t "green" w p pt 5
pause 0.2
reread
