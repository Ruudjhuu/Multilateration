################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/Application.cpp \
../src/Communication.cpp \
../src/Config.cpp \
../src/Mlat.cpp \
../src/Multilateration.cpp \
../src/Processor.cpp \
../src/Receiver.cpp \
../src/Sensor.cpp \
../src/Sensors.cpp \
../src/TimeMessage.cpp 

OBJS += \
./src/Application.o \
./src/Communication.o \
./src/Config.o \
./src/Mlat.o \
./src/Multilateration.o \
./src/Processor.o \
./src/Receiver.o \
./src/Sensor.o \
./src/Sensors.o \
./src/TimeMessage.o 

CPP_DEPS += \
./src/Application.d \
./src/Communication.d \
./src/Config.d \
./src/Mlat.d \
./src/Multilateration.d \
./src/Processor.d \
./src/Receiver.d \
./src/Sensor.d \
./src/Sensors.d \
./src/TimeMessage.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -std=c++14 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


