/*
 * Sensor.h
 *
 *  Created on: Nov 30, 2017
 *      Author: ruud
 */

#ifndef SENSOR_H_
#define SENSOR_H_

struct Pos{
	int x;
	int y;
};

class Sensor {
public:
	//Constructor
	Sensor(uint id, int x, int y);
	//Deconstructor
	virtual ~Sensor();

	//Functions
	std::string ToString();
	void Print();

	//Getters
	unsigned int GetId();
	Pos GetPos();
	int GetTime_stamp();
	int GetTime_correction();

	//Setters
	void SetRaw_time_stamp(int raw_time_stamp);
	void SetTime_correction(int time_correction);

private:
	unsigned int m_id;
	Pos m_pos;
	int m_raw_time_stamp;
	int m_time_correction;
};

#endif /* SENSOR_H_ */
