/*
 * Receiver.cpp
 *
 *  Created on: Dec 18, 2017
 *      Author: ruud
 */
#include <iostream>

#include "Receiver.h"
#include "fd_io.h"


Receiver::Receiver(){

}

Receiver::Receiver(std::shared_ptr<Processor> processor) {
	this->m_processor = processor;

}

void Receiver::RunBlocking(){
	std::clog << "Reciever: Running" << std::endl;
	while(true)
	{
		this->GetTimeMessage();

		this->PrintRecords();
		m_processor->ProcessReports(m_reports);

		// log the register to std::cerr
	}

}

bool Receiver::GetTimeMessage(){
	TimeMessage message_tmp = this->m_communication.ReceiveTime();
	if(message_tmp.id == 0)
	{
		return false;
	}
	m_reports[message_tmp.id] = message_tmp.time_stamp;
	return true;
}

void Receiver::PrintRecords(){
	for (auto entry : m_reports) {
		std::clog << entry.first << "=" << entry.second << std::endl;
	}
}
