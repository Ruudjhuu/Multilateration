/*
 * Mlat.cpp
 *
 *  Created on: Dec 18, 2017
 *      Author: ruud
 */

#include <iostream>
#include <fstream>
#include <sstream>

#include "Mlat.h"
#include "Eigen/Dense"
#include "Sensors.h"

void Mlat::MlatMethod(std::vector<std::shared_ptr<Sensor>>sensors){
	Eigen::MatrixX2d mat_A;
	Eigen::VectorXd vec_b;
	Eigen::Vector2d solution;
	int row = 0;
	for (unsigned int a = 0; a < sensors.size() - 2; ++a){
		for(unsigned int b = a+1; b < sensors.size() - 1; ++b){
			for(unsigned int c = b+1; c<sensors.size(); ++c){
				std::shared_ptr<Sensor> sens_i = sensors.at(a);
				std::shared_ptr<Sensor> sens_j = sensors.at(b);
				std::shared_ptr<Sensor> sens_k = sensors.at(c);
				Pos i = sens_i->GetPos();
				Pos j = sens_j->GetPos();
				Pos k = sens_k->GetPos();

				//TODO calculate these with tdoa.
				double r_ik = SPEED_OF_SOUND*(sens_i->GetTime_stamp() - sens_k->GetTime_stamp());
				double r_ij = SPEED_OF_SOUND*(sens_i->GetTime_stamp() - sens_j->GetTime_stamp());
				double r_ik2 = pow(r_ik,2.0);
				double r_ij2 = pow(r_ij,2.0);

				double ix2 = pow(i.x,2.0);
				double iy2 = pow(i.y,2.0);
				double jx2 = pow(j.x,2.0);
				double jy2 = pow(j.y,2.0);
				double kx2 = pow(k.x,2.0);
				double ky2 = pow(k.y,2.0);

				double a1 = (r_ik*j.x)-(r_ik*i.x)-(r_ij*k.x)+(r_ij*i.x);
				double a2 = (r_ik*j.y)-(r_ik*i.y)-(r_ij*k.y)+(r_ij*i.y);

				double b1 = 	r_ij*(r_ik2+ix2-kx2+iy2-ky2)/2.0 -
								r_ik*(r_ij2+ix2-jx2+iy2-jy2)/2.0;

				mat_A.conservativeResize( row + 1, 2 );
				vec_b.conservativeResize( row +1);
				mat_A(row, 0) = a1;
				mat_A(row, 1) = a2;
				vec_b(row, 0) = b1;
				++row;

			}
		}
	}

	std::clog << "A :" << std::endl << mat_A << std::endl;
	std::clog << "b :" << std::endl << vec_b << std::endl;
	std::clog << std::endl;
	solution = mat_A.colPivHouseholderQr().solve(vec_b);
	std::clog << "Solution:" << std::endl << solution << std::endl;
	Pos position;
	position.x = solution.coeff(0);
	position.y = solution.coeff(1);
	Mlat::ToFile(sensors, position);
}

void Mlat::ToFile(std::vector<std::shared_ptr<Sensor>> sensors, Pos position){
	std::ofstream file;
	std::string message;
	for (uint i = 0; i < sensors.size(); ++i){
		message = message + sensors.at(i)->ToString() + "\n";
	}
	double x = (double)position.x / (double)1000000;
	double y = (double)position.y / (double)1000000;
	message = message + std::to_string(x) + " " + std::to_string(y) + " red";
	file.open("mlat.out");
	file << message;
	file.close();
}
