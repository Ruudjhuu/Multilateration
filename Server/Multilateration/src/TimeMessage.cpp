/*
 * TimeMessage.cpp
 *
 *  Created on: Dec 5, 2017
 *      Author: ruud
 */

#include "TimeMessage.h"

std::ostream& operator<<(std::ostream& os, const TimeMessage& tm){
    return os << tm.id << " " << tm.time_stamp << std::endl;
}

