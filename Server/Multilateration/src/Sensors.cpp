/*
 * Sensors.cpp
 *
 *  Created on: Nov 30, 2017
 *      Author: ruud
 */
#include <stdexcept>
#include <iostream>
#include <cmath>

#include "Sensors.h"
#include "Mlat.h"

Sensors::Sensors() {
	this->m_sensors = std::vector<std::shared_ptr<Sensor>>();
	this->m_communication = std::make_unique<Communication>();
}

Sensors::~Sensors() {
}

void Sensors::AddSensor(unsigned int id, int x, int y) {
	this->m_sensors.push_back(std::make_shared<Sensor>(id, x, y));
}

std::shared_ptr<Sensor> Sensors::GetSensor(unsigned int id){
	for(unsigned int i = 0; i < this->m_sensors.size(); ++i){
		if(this->m_sensors.at(i)->GetId() == id)
			return this->m_sensors.at(i);
	}
	throw std::invalid_argument("Sensor: GetSensor: No sensor with given id: " + std::to_string(id));
}

void Sensors::Calibrate(std::map<int,long> reports) {
	if(reports.size() != this->m_sensors.size()){throw new std::runtime_error("Calibrating: Not enough unique messages");}

	std::clog << "Calibrating..." << std::endl;
	this->UpdateTimeStamps(reports);
	//Get posistion of emitter
	Pos e;
	const Pos s1 = this->GetSensor(1)->GetPos();
	const Pos s2 = this->GetSensor(2)->GetPos();
	e.x=(s1.x+s2.x)/2;
	e.y=(s1.y+s2.y)/2;

	//TODO calibrate loop.
	for (unsigned int i = 0; i < this->m_sensors.size(); ++i){
		std::shared_ptr<Sensor> sensor = this->m_sensors.at(i);
		Pos sensor_pos = sensor->GetPos();
		double distance = sqrt(pow(sensor_pos.x - e.x,2) + pow(sensor_pos.y - e.y,2));
		double expected_time = distance / SPEED_OF_SOUND;
		double time_correction = sensor->GetTime_stamp()-expected_time;
		sensor->SetTime_correction(time_correction);
	}
	std::clog << "Calibrating succesful" << std::endl;
	this->Print();
}

void Sensors::Mlat(std::map<int,long> reports){
	this->UpdateTimeStamps(reports);
	if ( !TimeDifferencesAreSane()) {
		return;
	}
	Mlat::MlatMethod(this->m_sensors);
}

bool Sensors::TimeDifferencesAreSane() {
	long max=INT64_MIN; //= std::max_element(times.begin(), times.end())[0];
	long min=INT64_MAX; //= std::min_element(times.begin(), times.end())[0];
	long ref=(*m_sensors.begin())->GetTime_stamp();
	for(auto s : m_sensors) {
		if ( s->GetTime_stamp() < min ) min=s->GetTime_stamp();
		if ( s->GetTime_stamp() >max ) max=s->GetTime_stamp();
	}
	std::clog << "min: " << min << std::endl << "max: " << max << std::endl;
	if((max - min) < MAX_TIME_DIFFERENCE){
		std::clog << "Processor: CkeckTimeDifferences: return true" << std::endl;
		this->Print();
		for(auto s : m_sensors) {
			std::clog << "delta to 1: " << s->GetTime_stamp()-ref << "/" << ref - s->GetTime_stamp() << std::endl;
		}
		return true;
	}
	return false;
}

void Sensors::UpdateTimeStamps(std::map<int,long> reports){
	for (auto entry : reports) {
			this->GetSensor(entry.first)->SetRaw_time_stamp(entry.second);
		}
}
void Sensors::Print(){
	std::clog << "Sensors information: " << std::endl;
	for(unsigned int i = 0; i < this->m_sensors.size(); ++i){
		this->m_sensors.at(i)->Print();
	}
	std::clog << "TDOA between 1 and 2: "
				<< this->m_sensors.at(0)->GetTime_stamp() - this->m_sensors.at(1)->GetTime_stamp()
				<< std::endl;
}

uint Sensors::size(){
	return m_sensors.size();
}
