/*
 * Sensor.cpp
 *
 *  Created on: Nov 30, 2017
 *      Author: ruud
 */
#include <iostream>

#include "Sensor.h"

//Constructor
Sensor::Sensor(uint id, int x, int y) {
	if(id<0){throw std::invalid_argument("id can't be negative");}
	this->m_id = id;
	this->m_pos.x = x;
	this->m_pos.y = y;
	this->m_raw_time_stamp = 0;
	this->m_time_correction = 0;
}

//Destructor
Sensor::~Sensor() {

}

//Getters
unsigned int Sensor::GetId(){
	return this->m_id;
}

Pos Sensor::GetPos(){
	return this->m_pos;
}

int Sensor::GetTime_stamp(){
	return this->m_raw_time_stamp - this->m_time_correction;
}

int Sensor::GetTime_correction(){
	return this->m_time_correction;
}

void Sensor::SetRaw_time_stamp(int time_stamp){
	this->m_raw_time_stamp = time_stamp;
}
//Setters
void Sensor::SetTime_correction(int time_correction){
	this->m_time_correction = time_correction;
}

void Sensor::Print(){
	std::clog << "Sensor: "
				"id: " << this->m_id <<
				" x: " << this->m_pos.x <<
				" y: " << this->m_pos.y <<
				" ts: " << this->GetTime_stamp() <<
				" tc " << this->m_time_correction <<
				std::endl;
}


//idiot name
std::string Sensor::ToString(){
	std::string rtn;
	double x = (double)this->m_pos.x / (double)1000000; // from um to m -> ugly
	double y = (double)this->m_pos.y / (double)1000000;
	rtn = std::to_string(x) + " " + std::to_string(y) + " green"; // green = plot color
	return rtn;
}

