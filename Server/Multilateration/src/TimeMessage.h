/*
 * TimeMessage.h
 *
 *  Created on: Dec 5, 2017
 *      Author: ruud
 */

#include <iostream>

#ifndef TIMEMESSAGE_H_
#define TIMEMESSAGE_H_

struct TimeMessage {
	TimeMessage() : id(0), time_stamp(0) {}
	int id;
	int time_stamp;
};

std::ostream& operator<<(std::ostream& os, const TimeMessage& tm);

#endif /* TIMEMESSAGE_H_ */
