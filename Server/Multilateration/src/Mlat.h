/*
 * Mlat.h
 *
 *  Created on: Dec 18, 2017
 *      Author: ruud
 */

#ifndef MLAT_H_
#define MLAT_H_

#include <memory>
#include <vector>

#include "Sensor.h"

namespace Mlat{

void MlatMethod(std::vector<std::shared_ptr<Sensor>> sensors);
void ToFile(std::vector<std::shared_ptr<Sensor>> sensors, Pos position);

}
#endif /* MLAT_H_ */
