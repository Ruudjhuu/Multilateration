/*
 * Config.cpp
 *
 *  Created on: Nov 30, 2017
 *      Author: ruud
 */

#include "Config.h"
#include "Sensor.h"

std::shared_ptr<Config> instance;

Config::Config() {
	this->m_file = std::ifstream();
	this->m_sensors = std::make_unique<Sensors>();
}

std::shared_ptr<Config> Config::Instance(){
	if(!instance)
		instance = std::make_shared<Config>();
	return instance;
}

void Config::LoadData(){
	this->OpenFile();
	std::string tmp_string = "";
	int line = 0;
	while (std::getline(this->m_file, tmp_string)) {
		if(tmp_string.size() > 0)
			if(tmp_string.at(0) != COMMENT_CHAR)
				this->StringToData(tmp_string, line);
		++line;
	}
	this->CloseFile();
	std::clog << CONFIG_NAME << ": succesfully loaded" << std::endl;
}

std::unique_ptr<Sensors> Config::GetSensors(){
	return std::move(this->m_sensors);
}

void Config::OpenFile() {
	this->m_file.open(CONFIG_NAME);
	if(this->m_file.fail())
	{
		std::cerr << CONFIG_NAME << ": Failed to open: " << std::strerror(errno) << std::endl;
	}
}

void Config::CloseFile() {
	this->m_file.close();
}

void Config::StringToData(std::string string, int line) {
	std::string name = string.substr(0, string.find(' '));
	try{
		if (name == "sensor"){
			this->CreateAntenna(string);
		}
		else{
			std::cerr << CONFIG_NAME << ": Unkown name: \"" << name << "\" at line: " << line << std::endl;
		}
	}
	catch(...)
	{
		std::cerr << CONFIG_NAME << ": Wrong syntax: at line: " << line << std::endl;
	}
}

void Config::CreateAntenna(std::string string)
{
	int id,x,y,begin_value;
	begin_value = string.find(' ') + 1;
	std::stringstream values_str(string.substr(begin_value));
	values_str >> id >> x >> y;
	this->m_sensors->AddSensor(id,x,y);
	std::clog << "Created sensor: " ;
	this->m_sensors->GetSensor(id)->Print();
}
