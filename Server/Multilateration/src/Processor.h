/*
 * Processor.h
 *
 *  Created on: Dec 18, 2017
 *      Author: ruud
 */

#ifndef PROCESSOR_H_
#define PROCESSOR_H_


#include <map>

#include "Sensors.h"
#include "Config.h"

class Processor {
	public:
		Processor();
		void ProcessReports(const std::map<int,long>& reports);
	private:
		void LoadConfig();

		bool m_calibrated;
		std::unique_ptr<Sensors> m_sensors;

};

#endif /* PROCESSOR_H_ */
