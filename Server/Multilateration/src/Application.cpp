/*
 * Application.cpp
 *
 *  Created on: Nov 30, 2017
 *      Author: ruud
 */

#include "Application.h"

Application::Application() {
	this->m_processor = std::make_shared<Processor>();
	this->m_receiver = Receiver(m_processor);
}

Application::~Application() {

}

void Application::Run() {
	while(true){
		this->m_receiver.RunBlocking();
	}
}

