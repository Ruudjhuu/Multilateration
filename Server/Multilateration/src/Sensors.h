/*
 * Sensors.h
 *
 *  Created on: Nov 30, 2017
 *      Author: ruud
 */
#include <vector>
#include <memory>
#include <map>

#include "Sensor.h"
#include "Communication.h"

#ifndef SENSORS_H_
#define SENSORS_H_

#define SPEED_OF_SOUND 343 //needs to add up with multilateration.conf
#define MAX_TIME_DIFFERENCE 100000 //us //100 ms //0.1 s

class Sensors {
public:
	Sensors();
	virtual ~Sensors();
	void AddSensor(unsigned int id, int x, int y);
	std::shared_ptr<Sensor> GetSensor(unsigned int id);
	void Calibrate(std::map<int,long> reports);
	void Mlat(std::map<int,long> reports);
	void Print();
	uint size();

private:
	void UpdateTimeStamps(std::map<int,long> reports);
	bool TimeDifferencesAreSane();
	std::vector<std::shared_ptr<Sensor>> m_sensors;
	std::unique_ptr<Communication> m_communication;
};

#endif /* SENSORS_H_ */
