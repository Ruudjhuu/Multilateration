/*
 * Receiver.h
 *
 *  Created on: Dec 18, 2017
 *      Author: ruud
 */

#ifndef RECEIVER_H_
#define RECEIVER_H_

#include <memory>
#include <map>

#include "Processor.h"
#include "TimeMessage.h"
#include "Communication.h"


class Receiver {
public:
	Receiver();
	Receiver(std::shared_ptr<Processor> processor);
	void RunBlocking();

private:
	bool GetTimeMessage();
	void PrintRecords();

	std::shared_ptr<Processor> m_processor;
	std::map<int, long> m_reports;
	Communication m_communication;
};

#endif /* RECEIVER_H_ */
