/*
 * Communication.cpp
 *
 *  Created on: Nov 30, 2017
 *      Author: ruud
 */

#include <limits>

#include "Communication.h"

Communication::Communication() {}

Communication::~Communication() {}

TimeMessage Communication::ReceiveTime(){
	TimeMessage time_message_tmp;
	TimeMessage time_message;

	bool message_good = true;
	char begin, end;
	std::cin >> begin;
	if (begin != BEGIN_CHAR){
		std::clog << "Wrong message syntax" <<  (unsigned)begin <<std::endl;
		message_good = false;
	}
	std::cin >> time_message_tmp.id;
	std::cin >> time_message_tmp.time_stamp;
	std::cin >> end;
	if (end != END_CHAR){
		std::clog << "Wrong message syntax" << (unsigned)end <<std::endl;
		message_good = false;
	}
	std::clog << "Received: struct TimeMessage: " << time_message_tmp << std::endl;
	if(message_good)
	{
		time_message = time_message_tmp;
	}
	return time_message;
}
