/*
 * Processor.cpp
 *
 *  Created on: Dec 18, 2017
 *      Author: ruud
 */

#include <vector>
#include <algorithm>

#include "Processor.h"

Processor::Processor() {
	this->m_calibrated = false;
	this->LoadConfig();
}

void Processor::ProcessReports(const std::map<int,long>& reports){
	if(!this->m_calibrated){
		if(reports.size() == m_sensors->size()){
			m_sensors->Calibrate(reports);
			m_calibrated = true;
		}
	}
	else{
		m_sensors->Mlat(reports);
	}
}

void CheckReportSet(const std::map<int, long>& reports){
}

void Processor::LoadConfig(){
	Config::Instance()->LoadData();
	this->m_sensors = Config::Instance()->GetSensors();
}
