//============================================================================
// Name        : Multilateration.cpp
// Author      : Ruud Swinkels
// Version     :
// Copyright   : 
// Description : Hello World in C++, Ansi-style
//============================================================================
#include <iostream>
#include <memory>

#include "Application.h"
#include "Communication.h"

int main() {
	std::unique_ptr<Application> application = std::make_unique<Application>();
	application->Run();
	return 0;
}
