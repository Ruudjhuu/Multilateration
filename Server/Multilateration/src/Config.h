/*
 * Config.h
 *
 *  Created on: Nov 30, 2017
 *      Author: ruud
 */
#include <string>
#include <map>
#include <memory>
#include <iostream>
#include <fstream>
#include <cstring>
#include <cerrno>
#include <sstream>

#include "Sensors.h"


#ifndef CONFIG_H_
#define CONFIG_H_

#define CONFIG_NAME "multilateration.conf"
#define COMMENT_CHAR '#'

class Config{
public:
	//Constructor
	Config();
	//Functions
	static std::shared_ptr<Config> Instance();
	void LoadData();
	std::unique_ptr<Sensors> GetSensors();


	Config(Config const&) = delete;
	void operator=(Config const&) = delete;


private:
	//methods
	void OpenFile();
	void CloseFile();
	void StringToData(std::string string, int line);
	void CreateAntenna(std::string string);

	//fields
	static std::shared_ptr<Config> m_instance;
	std::ifstream m_file;
	std::unique_ptr<Sensors> m_sensors;
};
#endif /* CONFIG_H_ */
