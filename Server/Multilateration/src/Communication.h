/*
 * Communication.h
 *
 *  Created on: Nov 30, 2017
 *      Author: ruud
 */
#include <iostream>
#include <string>

#include "TimeMessage.h"

#ifndef COMMUNICATION_H_
#define COMMUNICATION_H_

#define BEGIN_CHAR '%'
#define END_CHAR '#'

class Communication {
public:
	Communication();
	virtual ~Communication();
	TimeMessage ReceiveTime();

private:
	void Connect();
};

#endif /* COMMUNICATION_H_ */
