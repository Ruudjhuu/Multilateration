/*
 * Application.h
 *
 *  Created on: Nov 30, 2017
 *      Author: ruud
 */
#include <vector>
#include <memory>

#include "Processor.h"
#include "Receiver.h"

#ifndef APPLICATION_H_
#define APPLICATION_H_

class Application {
public:
	Application();
	virtual ~Application();
	void Run();

private:
	//Functions

	//fields
	std::shared_ptr<Processor> m_processor;
	Receiver m_receiver;
};

#endif /* APPLICATION_H_ */
