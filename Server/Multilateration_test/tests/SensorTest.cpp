#include <gtest/gtest.h>
#include <memory>

#include "Sensor.h"

#define	ID 93
#define X 3290
#define Y 9263

namespace {
	class SensorTest : public testing::Test {
	protected:
		std::unique_ptr<Sensor> sensor;

		virtual void SetUp() {
			sensor = std::make_unique<Sensor>(ID,X,Y);
		}

		void ConstructerTest(int id, int x, int y){
			sensor = std::make_unique<Sensor>(id,x,y);
			ASSERT_EQ(id, sensor->GetId());
			ASSERT_EQ(x, sensor->GetPos().x);
			ASSERT_EQ(y, sensor->GetPos().y);
			EXPECT_EQ(0, sensor->GetTime_stamp());
		}
	};
	TEST_F(SensorTest, Constructor_positive_values) {
		ASSERT_NO_THROW(ConstructerTest(ID,X,Y));
	}

	TEST_F(SensorTest, Constructor_null_values) {
		ASSERT_NO_THROW(ConstructerTest(0,0,0));
	}

	TEST_F(SensorTest, Constructor_negative_values) {
		EXPECT_THROW(ConstructerTest(-1,-1,-1),std::invalid_argument);
	}

	TEST_F(SensorTest, Constructor_negative_values_except_id) {
		ASSERT_NO_THROW(ConstructerTest(0,-1,-1));
	}

	TEST_F(SensorTest, GetId) {
		ASSERT_EQ(ID, sensor->GetId());
	}

	TEST_F(SensorTest, GetPos) {
		ASSERT_EQ(X, sensor->GetPos().x);
		ASSERT_EQ(Y, sensor->GetPos().y);
	}

	TEST_F(SensorTest, GetTime_stamp)
	{
		ASSERT_EQ(0, sensor->GetTime_stamp());
	}

	TEST_F(SensorTest, GetTime_correction){
		ASSERT_EQ(0, sensor->GetTime_correction());
	}

	TEST_F(SensorTest, SetRaw_time_stamp_positive_value){
		int value = 892;
		sensor->SetRaw_time_stamp(value);
		ASSERT_EQ(value, sensor->GetTime_stamp());
	}

	TEST_F(SensorTest, SetRaw_time_stamp_null_value){
		int value = 0;
		sensor->SetRaw_time_stamp(value);
		ASSERT_EQ(value, sensor->GetTime_stamp());
	}

	TEST_F(SensorTest, SetRaw_time_stamp_negative_value){
		int value = -1;
		sensor->SetRaw_time_stamp(value);
		ASSERT_EQ(value, sensor->GetTime_stamp());
	}

	TEST_F(SensorTest, SetTime_correction_positive_value){
		int value = 34937;
		sensor->SetTime_correction(value);
		ASSERT_EQ(value, sensor->GetTime_correction());
	}
}
