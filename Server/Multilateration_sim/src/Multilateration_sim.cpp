//============================================================================
// Name        : Multilateration_sim.cpp
// Author      : Ruud Swinkels
// Version     :
// Copyright   : 
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <map>
#include <unistd.h>
#include <cmath>
#include <vector>

#define SPEED_OF_SOUND 343

struct Pos{
	Pos(int x, int y):x(x),y(y){}
	int x;
	int y;
};

std::map<int, Pos*> antennas;
std::vector<Pos*> emitors;
std::map<int, double> expected_times;
uint emitor_count = 0;

void CreateSensorPositions(){
	antennas[1] = new Pos(5000000,6000000);
	antennas[2] = new Pos(5000000,8000000);
	antennas[3] = new Pos(6500000,12000000);
	antennas[4] = new Pos(17000000,15000000);
	antennas[5] = new Pos(13500000,2000000);

}

void CreateEmitors(){
	emitors.push_back(new Pos(5000000,7000000)); //must be between pos antenna 1 and 2

	emitors.push_back(new Pos(0,0));
	emitors.push_back(new Pos(1250000,5000000));
	emitors.push_back(new Pos(2500000,10000000));
	emitors.push_back(new Pos(3750000,15000000));
	emitors.push_back(new Pos(5000000,20000000));
	emitors.push_back(new Pos(6250000,15000000));
	emitors.push_back(new Pos(7500000,10000000));
	emitors.push_back(new Pos(8750000,5000000));
	emitors.push_back(new Pos(10000000,0));

	emitors.push_back(new Pos(11250000,5000000));
	emitors.push_back(new Pos(12500000,10000000));
	emitors.push_back(new Pos(13750000,15000000));
	emitors.push_back(new Pos(15000000,20000000));
	emitors.push_back(new Pos(16250000,15000000));
	emitors.push_back(new Pos(17500000,10000000));
	emitors.push_back(new Pos(18750000,5000000));
	emitors.push_back(new Pos(20000000,0));
}

void getExpectedTimes(){
	for (uint i = 1; i <= antennas.size(); ++i){
		Pos* pos_a = antennas.at(i);
		Pos* pos_e = emitors.at(emitor_count);
		double distance = sqrt(pow(pos_a->x - pos_e->x,2) + pow(pos_a->y - pos_e->y,2));
		double expected_time = distance / SPEED_OF_SOUND;
		expected_times[i] = expected_time;
	}
}

void sendExpectedTimes(){
	for (auto entry : expected_times){
		std::cout << "%"
				<< entry.first
				<< " "
				<< (long long)entry.second
				<< "# "
				<< std::flush; // flush is needed for sleep to work.
	}
}

void Setup(){
	CreateSensorPositions();
	CreateEmitors();
}

void Loop(){
	getExpectedTimes();
	sendExpectedTimes();
	emitor_count++;
	if (emitor_count == emitors.size()) emitor_count=0;
	sleep(1);
}

int main() {
	Setup();
	while(true){
		Loop();
	}
}
