# Multilateration
## Inleiding

Dit is een stage project bij 42 solutions over multilateration. Het project is uitgevoerd door Ruud Swinkels. De opdracht voor dit project luid: <br \>
Het doel van deze opdracht is om inzichtelijk te krijgen welke randvoorwaarden er zijn om een redelijk tot goede positiebepaling te doen op basis van multilateration. De ultieme wens is te komen tot een opstelling waarmee in een lokaal de positie kan worden bepaald van een geluidsbron (spreker). Omdat het geluidssignaal relatief traag is, in vergelijking met radiosignalen in het luchtverkeer, is het de hoop dat de invloed van tijd-meetfouten kan worden beperkt en kan worden onderzocht.

## Contact
Mochten er vragen zijn over dit project kun je een mail sturen naar:<br \>
RuudSwinkels@hotmail.nl<br \>
<br \>
Andere contact mogelijkheden mogen ook, mochten deze gegevens al in bezit zijn.

## Repo Indeling
* demo:<br \>
  alle benodigde bestanden en scripts om een demo te geven. Er bestaat een demo die gebruik maakt met hardware en een demo die gebruik maakt van simulatie.
* doc:<br \>
  Alle documentatie betreft het project.
* onderzoek_tests:<br \>
  Source code van kleine tests die gedaan zijn voor het onderzoek.
* presentations:<br \>
  Alle presentaties die gegeven zijn betreft dit project.
* scripts:<br \>
  Handige scripts die gebruikt zijn voor het maken van de demo.
* Sensor:<br \>
  Source code voor de sensor module.
* Server:<br \>
  Source code voor de server, server tests en simulatie.

## Gebruik
### Simulatie Demo
Om de simulatie te starten kunnen de volgende comando's gebruikt worden in de directory: /demo/simulation.<br \><br \>
* ./Start_gui<br \>
Start de grafische interface.
* ./Start_server<br \>
Start de mlat server welke berichten krijgt van de simulatie.
* ./Start_hardware_sim<br \>
Laat de simulatie beginnen met het versturen van berichten naar de server.
* ./Show_hardware_sim_output<br \>
Laat de ruwe data zien die de server gebruikt.
<br \><br \>
Deze commando's kunnen in de genoteerde volgorde uitgevoerd worden. Start_gui en Show_hardware_sim_output zijn optioneel. Het is noodzakelijk om Start_hardware_sim **na** Start_server te runnen in verband met het ijk proces.

### Hardware Demo Server side

Om de demo te starten kunnen de volgende comando's gebruikt worden in de directory: /demo/hardware.<br \><br \>
* ./Connect_wifi<br \>
Connect met "Mlat" WIFI doormiddel van "iwconfig" (dit is alleen nodig wanneer op een andere manier connecten problemen veroorzaakt)
* ./Show_connections<br \>
Laat alle ESTABLISHED connecties zien op poort 8080. 
* ./Show_input<br \>
Laat de rauwe data zien die van de sensoren af komen.
* ./Start_server<br \>
Start de server die luistert op poort 8080.
<br \>
<br \>
Deze commando's kunnen in de genoteerde volgorde uitgevoerd worden. Connect_wifi, Show_connections en Show_input zijn optioneel. De server gaat meteen in de staat dat er geijkt kan worden.<br />

### Configuratie
In de /demo/hardware directory zit een multilateration.conf bestand. In dit bestand staan de locaties van de sensoren genoteerd in um (micrometers). De volgende syntax is toegepast:<br />
sensor x-coordinaat y-coordinaat<br />
voorbeeld:<br />
sensor 100000 2000000 <br />

#### IJken
IJken wordt gedaan doormiddel van geluid te maken op de locatie tussen sensor 1 en sensor 2. Hoe de sensor id is in te stellen wordt behandeld in het kopje Hardware Demo Sensor Side. De eerste berichten worden gebruikt voor het ijken. De sensoren mogen tussen het starten van de server en het beeindigen van het ijkproces geen achtergrond geluid opvangen.

## Hardware Demo Sensor side
Het opzetten van de sensoren is een eenmalig proces. Mocht de sensor opnieuw worden opgestart zullen deze meteen werken.<br />

### Input en Output
De pin configuratie van de Seeeduion Cloud is als volgt:<br />
* De digitale pinnen D8, D9 en D10 zijn voor de Sensor ID configuration. Dit is bitwise gedaan. D8 is het eerste bitje (0b1) en D10 is het 3e bitje (0b100).
* Pin D7 moet aangesloten worden op pin D0 van de microfoon module. Dit is voor de interrupt
* + van de microfoon module moet aangesloten worden op 5V van de Seeeduino
* G van de microfoon module moet aangesloten worden op GND van de Seeeduino
<br />
Het schrofje op de blauwe potmeter van de Seeeduino is voor de gevoeligheid van de microfoon module. Clockwise is gevoeliger en counterclockwise is minder gevoelig.</br />

### Configuratie
#### Acces Point
De bestanden in /demo/hardware_config/acces_point moeten op 1 seeeduino geupload worden in /etc/config. Na de upload moet de Seeeeduino opnieuw opgestart worden.

#### Clients
De bestanden in /demo/hardware_config/clients/client_# moeten geupload worden in /etc/config van iedere seeeduino. Na de upload moet de Seeeeduino opnieuw opgestart worden.

### Code
De code in /Sensor moet door de officiele Arduino IDE (gebruikte versie 1.8.5) gecompiled worden en geupload op de sensor. De code is voor iedere sensor hetzelfde.




