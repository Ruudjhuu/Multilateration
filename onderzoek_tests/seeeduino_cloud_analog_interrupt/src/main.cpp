#include <Arduino.h>

bool flag = false;
int count = 0;

void setup()
{
  // initialize serial communications at 9600 bps:
  Serial.begin(9600);

  // clear ACSR
  ACSR |= (1<<ACI);

  // set ADC0 as negative input for comparator
  ADCSRA &= ~(1 << ADEN);
  ADCSRB |= (1 << ACME);
  ADMUX |= (1 << 0b000);

  ACSR =
    (0 << ACD) |    // Analog Comparator: Enabled
    (0 << ACBG) |   // Analog Comparator Bandgap Select: AIN0 is applied to the positive input
    (0 << ACO) |    // Analog Comparator Output: Off
    (1 << ACI) |    // Analog Comparator Interrupt Flag: Clear Pending Interrupt
    (1 << ACIE) |   // Analog Comparator Interrupt: Enabled
    (0 << ACIC) |   // Analog Comparator Input Capture: Disabled
    (1 << ACIS1) | (1 << ACIS0);   // Analog Comparator Interrupt Mode: Comparator Interrupt on Rising Output Edge

  pinMode(13, OUTPUT);  // toggles when ISR runs
  pinMode(9, OUTPUT);   // indicates status of comparator output
}

void loop()
{
  digitalWrite(13, flag);
  Serial.println(count);
  delay(100);
}

ISR(ANALOG_COMP_vect )
{
  flag = !flag;  // toggle state of Pin 13
  ++count;
}
