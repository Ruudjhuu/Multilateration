#include <Arduino.h>

int input = A0;
int result;
unsigned long time1 = 0;
unsigned long time2;

void setup() {
    Serial.begin(9600);
}

void loop() {
  time1 = micros();
  result = analogRead(input);
  time2 = micros();
  Serial.print("Tijdverschil (micro seconden): \t");
  Serial.print(time2 - time1);
  Serial.print(", Input waarde: \t");
  Serial.print(result);
  Serial.print("\n");
  delay(1000);
}
