//============================================================================
// Name        : Mlat_test.cpp
// Author      : Ruud Swinkels
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================
#include <iostream>
#include <vector>
#include <Eigen/Dense>
#include <stddef.h>
#include <cmath>

#include "Antenna.h"

typedef long double prec_t;
typedef Eigen::Matrix<prec_t, 2, 1 > vec2_t;
typedef Eigen::Matrix< double, Eigen::Dynamic, 2 > matD2_t;

void PrintAntennas(std::vector<Antenna*> &antennas){
	for (uint var = 0; var < antennas.size(); ++var) {
		antennas.at(var)->Print();
	}
}

void CreateDummyAntennas(std::vector<Antenna*> &antennas){
	antennas.push_back(new Antenna(0,0));
	antennas.push_back(new Antenna(6,1));
	antennas.push_back(new Antenna(2,5));
	antennas.push_back(new Antenna(8,2));
	std::cout << "Antennas created:" << std::endl;
	PrintAntennas(antennas);
	std::cout << std::endl;
}

double TDOA_distance( int pos_a[2], int pos_b[2]){
	int e[2] = {80,1165};
	double distand_ae = sqrt(pow((pos_a[0]-e[0]),2.0) + pow((pos_a[1]-e[1]),2.0));
	double distand_be = sqrt(pow(pos_b[0]-e[0],2.0)+ pow(pos_b[1]-e[1],2.0));
	double TDOA = distand_ae - distand_be;
	return TDOA;
}

void Mlat_Ruud(std::vector<Antenna*> &antennas){

	Eigen::Matrix2f A;
	Eigen::Vector2f b;

	for (int var = 0; var < 2; ++var) {
		int* i = antennas.at(0+var)->getPos();
		int* j = antennas.at(1+var)->getPos();
		int* k = antennas.at(2+var)->getPos();

		double r_ik = TDOA_distance(i,k);
		double r_ij = TDOA_distance(i,j);

		double r_ik2 = pow(r_ik,2.0);
		double r_ij2 = pow(r_ij,2.0);

		double ix2 = pow(i[0],2.0);
		double iy2 = pow(i[1],2.0);
		double jx2 = pow(j[0],2.0);
		double jy2 = pow(j[1],2.0);
		double kx2 = pow(k[0],2.0);
		double ky2 = pow(k[1],2.0);

		double a1 = (r_ik*j[0])-(r_ik*i[0])-(r_ij*k[0])+(r_ij*i[0]);
		double a2 = (r_ik*j[1])-(r_ik*i[1])-(r_ij*k[1])+(r_ij*i[1]);

		double b1 = 	r_ij*(r_ik2+ix2-kx2+iy2-ky2)/2.0 -
						r_ik*(r_ij2+ix2-jx2+iy2-jy2)/2.0;

		A(var, 0) = a1;
		A(var, 1) = a2;
		b(var) = b1;
	}

	std::cout << A << std::endl << std::endl;
	std::cout << "Solution:" << std::endl << A.colPivHouseholderQr().solve(b) <<std::endl;

}

int main() {
	std::vector<Antenna*> antennas;
	CreateDummyAntennas(antennas);
	Mlat_Ruud(antennas);
	//Mlat_Walter(antennas);

}

/*
void Mlat_Walter(std::vector<Antenna*> &antennas){

	int xi = antennas.at(0)->getPos()[0];
	int yi = antennas.at(0)->getPos()[1];
	int xj = antennas.at(1)->getPos()[0];
	int yj = antennas.at(1)->getPos()[1];
	int xk = antennas.at(2)->getPos()[0];
	int yk = antennas.at(2)->getPos()[1];
	int xe = 4;
	int ye = 4;

	prec_t Xi = xi, Yi = yi;
	prec_t Xj = xj, Yj = yj;
	prec_t Xk = xk, Yk = yk;

	prec_t Rij = sqrt(pow(xi-xe,2.0)+ pow(yi-ye,2.0))-sqrt(pow(xj-xe,2.0)+ pow(yj-ye,2.0));
	prec_t Rik = sqrt(pow(xi-xe,2.0)+ pow(yi-ye,2.0))-sqrt(pow(xk-xe,2.0)+ pow(yk-ye,2.0));

	prec_t Rij2 = std::pow( Rij, 2.0 );
	prec_t Rik2 = std::pow( Rik, 2.0 );


	prec_t Xji = Xj - Xi, Yji = Yj - Yi;
	prec_t Xki = Xk - Xi, Yki = Yk - Yi;

	prec_t Xi2 = std::pow( Xi, 2.0 ), Yi2 = std::pow( Yi, 2.0 );
	prec_t Xj2 = std::pow( Xj, 2.0 ), Yj2 = std::pow( Yj, 2.0 );
	prec_t Xk2 = std::pow( Xk, 2.0 ), Yk2 = std::pow( Yk, 2.0 );

	vec2_t ARow( Rij * Xki - Rik * Xji, Rij * Yki - Rik * Yji);
	prec_t bRow = Rik * ( Rij2 + Xi2 - Xj2 + Yi2 - Yj2) /
						prec_t( 2.0 ) -
				  Rij * ( Rik2 + Xi2 - Xk2 + Yi2 - Yk2) /
						prec_t( 2.0 );

	std::cout << "ARow: " << std::endl << ARow << std::endl;
	std::cout << std::endl;
	std::cout << "bRow: "<<bRow << std::endl;
	std::cout << std::endl;
}*/
