#include <Arduino.h>
#include <Bridge.h>
#include <BridgeClient.h>
#include <string.h>

#define BAUDRATE 9600
#define PORT 255
#define IP {192,168,240,1}

BridgeClient client;
bool clientActive = false;
String tmpString;

void setup()
{
  delay(3000); //needed for debuging because PIO device monitor is fucking slow
  Serial.begin(BAUDRATE);
  Serial.println("Setup begin");

  Serial.println("Bridge startup (2 seconds)");
  Bridge.begin();

  Serial.println("Setup ended");
}

void loop() {
  //Check if client is connected
  if (client.connected())
  {
	//If there was no client active, set active boolean to true
    if (!clientActive)
    {
      clientActive = true;
      Serial.println("Connected with server");
    }

    //if client buffer is available, print received message
    if (client.available())
    {
      Serial.print("Received: ");
      tmpString = client.readString();
      Serial.print(tmpString);
    }

    //if serial buffer is available, send message and print sneded message
    if(Serial.available())
    {
      Serial.print("Send: ");
      tmpString = Serial.readString();
      Serial.print(tmpString);
      client.print(tmpString);
    }
  }
  //if there is no connection, try to connect.
  else
  {
    //if there was a connection, stop client and set connect boolean to false
    if(clientActive)
    {
      client.stop();
      Serial.println("Disconnected with server");
      clientActive = false;
    }
    client.connect(IP, PORT);
  }
  //try to connect every second
  delay(1000);
}
