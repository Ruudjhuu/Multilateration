#include <iostream>
#include <memory>
#include <vector>
#include <Eigen/Dense>

#include "Antenna.h"

typedef std::unique_ptr<Antenna> antenna_ptr;
typedef std::vector<antenna_ptr> antenna_vec;

std::unique_ptr<antenna_vec> CreateDummyAntennas(std::unique_ptr<antenna_vec> antennas){
	antennas->push_back(std::make_unique<Antenna>(2,3));
	antennas->push_back(std::make_unique<Antenna>(8,5));
	antennas->push_back(std::make_unique<Antenna>(4,2));
	antennas->push_back(std::make_unique<Antenna>(1,9));
	return antennas;
}

double TDOA_distance( int pos_a[2], int pos_b[2]){
	int e[2] = {3,6};
	double distand_ae = sqrt(pow((pos_a[0]-e[0]),2.0) + pow((pos_a[1]-e[1]),2.0));
	double distand_be = sqrt(pow(pos_b[0]-e[0],2.0)+ pow(pos_b[1]-e[1],2.0));
	double TDOA = distand_ae - distand_be;
	return TDOA;
}

void Mlat(std::unique_ptr<antenna_vec> antennas){
	Eigen::MatrixX2d mat_A;
	Eigen::VectorXd vec_b;
	int row = 0;
	for (unsigned int a = 0; a < antennas->size() - 2; ++a){
		for(unsigned int b = a+1; b < antennas->size() - 1; ++b){
			for(unsigned int c = b+1; c<antennas->size(); ++c){
				int* i = antennas->at(a)->getPos();
				int* j = antennas->at(b)->getPos();
				int* k = antennas->at(c)->getPos();

				double r_ik = TDOA_distance(i,k);
				double r_ij = TDOA_distance(i,j);

				double r_ik2 = pow(r_ik,2.0);
				double r_ij2 = pow(r_ij,2.0);

				double ix2 = pow(i[0],2.0);
				double iy2 = pow(i[1],2.0);
				double jx2 = pow(j[0],2.0);
				double jy2 = pow(j[1],2.0);
				double kx2 = pow(k[0],2.0);
				double ky2 = pow(k[1],2.0);

				double a1 = (r_ik*j[0])-(r_ik*i[0])-(r_ij*k[0])+(r_ij*i[0]);
				double a2 = (r_ik*j[1])-(r_ik*i[1])-(r_ij*k[1])+(r_ij*i[1]);

				double b1 = 	r_ij*(r_ik2+ix2-kx2+iy2-ky2)/2.0 -
								r_ik*(r_ij2+ix2-jx2+iy2-jy2)/2.0;

				mat_A.conservativeResize( row + 1, 2 );
				vec_b.conservativeResize( row +1);
				mat_A(row, 0) = a1;
				mat_A(row, 1) = a2;
				vec_b(row, 0) = b1;
				++row;

			}
		}
	}

	std::cout << "A :" << std::endl << mat_A << std::endl;
	std::cout << "b :" << std::endl << vec_b << std::endl;
	std::cout << std::endl;
	std::cout << "Solution:" << std::endl << mat_A.colPivHouseholderQr().solve(vec_b) <<std::endl;
}

int main() {
	std::unique_ptr<antenna_vec> antennas = std::make_unique<antenna_vec>();
	antennas = CreateDummyAntennas(std::move(antennas));
	Mlat(std::move(antennas));
}
