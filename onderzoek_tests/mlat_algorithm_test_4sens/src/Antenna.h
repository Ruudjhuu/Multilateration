#include <iostream>
#include <vector>

#ifndef ANTENNA_H_
#define ANTENNA_H_

class Antenna {
public:
	Antenna(int x, int y);
	~Antenna();
	void Print();
	int* getPos();
	int getId();

private:
    int id;
    static int current_id;
	int pos[2];
};

#endif /* ANTENNA_H_ */
