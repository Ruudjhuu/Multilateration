#include "Antenna.h"

int Antenna::current_id;

Antenna::Antenna(int x, int y) {
	// TODO Auto-generated constructor stub
	this->id = current_id++;
	this->pos[0] = x;
	this->pos[1] = y;
}

Antenna::~Antenna()
{

}

int* Antenna::getPos(){return this->pos;}
int Antenna::getId(){return this->id;}

void Antenna::Print() {
    std::cout << "Antenna: " << this->id << " | ";
	std::cout << "(" << this->pos[0] << "," << this->pos[1] << ")" << std::endl;
}
