#include <Arduino.h>

int Led = 13 ;
int micpin = 7;
int val = 0;
volatile int count = 0;
int prev_count = 0;

void MicInterupt()
{
 static unsigned long last_interrupt_time = 0;
 unsigned long interrupt_time = millis();

 if (interrupt_time - last_interrupt_time > 10)
 {
   count++;
 }
 last_interrupt_time = interrupt_time;
}

void setup ()
{
  Serial.begin(9600);
  while (!Serial){}
  pinMode (Led, OUTPUT) ;
  pinMode (micpin, INPUT_PULLUP) ;
  attachInterrupt(digitalPinToInterrupt(micpin), MicInterupt, RISING);
}

void loop ()
{
  if(prev_count != count)
  {
    Serial.println(count);
    prev_count = count;
  }
}
