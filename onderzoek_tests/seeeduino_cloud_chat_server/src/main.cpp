#include <Arduino.h>
#include <Bridge.h>
#include <BridgeClient.h>
#include <BridgeServer.h>
#include <string.h>

#define BAUDRATE 9600
#define PORT 255

BridgeServer server(PORT);
BridgeClient client;
char output;
bool clientActive = false;
String tmpString;

void setup()
{
  delay(3000); //needed for debuging because PIO device monitor is fucking slow
  Serial.begin(BAUDRATE);
  Serial.println("Setup begin");

  Serial.println("Bridge startup (2 seconds)");
  Bridge.begin();

  Serial.println("Server noListenOnLocalhost");
  server.noListenOnLocalhost();

  Serial.println("Server begin");
  server.begin();

  Serial.println("Setup ended");
}

void loop() {
  //Check if client is connected
  if (client.connected())
  {
    //Check if there was an active client.
    //if not, let know a client is connected.
    if (!clientActive)
    {
      Serial.println("New client connection.");
      clientActive = true;
    }

    //Check if there is data in client buffer
    //if so, Print the data
    if (client.available())
    {
      Serial.print("Received: ");
      tmpString = client.readString();
      Serial.print(tmpString);
    }

    //Check if there is data in serial buffer
    //if so, Print the data
    if(Serial.available())
    {
      Serial.print("Send: ");
      tmpString = Serial.readString();
      Serial.print(tmpString);
      client.print(tmpString);
    }
    delay(100);
  }

  //if there is no connection, try to connect.
  else
  {
    //Check if client was active.
    //if so, let know client is disconnected.
    if (clientActive)
    {
      client.stop();
      Serial.println("Client disconnected.");
      clientActive = false;
    }

    client = server.accept();
  }
}
