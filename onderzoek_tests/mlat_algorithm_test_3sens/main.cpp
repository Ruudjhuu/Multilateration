#include <iostream>
#include <vector>
#include <Eigen/Dense>
#include <stddef.h>
#include <cmath>

#include "Antenna.h"

typedef long double prec_t;
typedef Eigen::Matrix<prec_t, 2, 1 > vec2_t;
typedef Eigen::Matrix< double, Eigen::Dynamic, 2 > matD2_t;

int ant_1[2] = {2,3};
int ant_2[2] = {5,8};
int ant_3[2] = {4,2};

void PrintAntennas(std::vector<int*> &antennas){
	for (uint var = 0; var < antennas.size(); ++var) {
		std::cout << antennas.at(var)[0] << ", " << antennas.at(var)[1] << std::endl;
	}
}

void CreateDummyAntennas(std::vector<int*> &antennas){

	antennas.push_back(ant_1);
	antennas.push_back(ant_2);
	antennas.push_back(ant_3);
	std::cout << "Antennas created:" << std::endl;
	PrintAntennas(antennas);
	std::cout << std::endl;
}

double TDOA_distance( int pos_a[2], int pos_b[2]){
	int e[2] = {3,6};
	double distand_ae = sqrt(pow((pos_a[0]-e[0]),2.0) + pow((pos_a[1]-e[1]),2.0));
	double distand_be = sqrt(pow(pos_b[0]-e[0],2.0)+ pow(pos_b[1]-e[1],2.0));
	double TDOA = distand_ae - distand_be;
	return TDOA;
}

void Mlat_Ruud(std::vector<int*> &antennas){

    Eigen::Matrix<float, 3, 2> A;
	Eigen::Vector3f b;

	int* c = antennas.at(0);
    int* d = antennas.at(1);
    int* e = antennas.at(2);

    int* i;
    int* j;
    int* k;

	for (int var = 0; var < 3; ++var) {
        switch (var){
            case 0:
                i = c;
                j = d;
                k = e;
                break;

            case 1:
                i = d;
                j = e;
                k = c;
                break;

            case 2:
                i = e;
                j = c;
                k = d;
                break;
        }

		double r_ik = TDOA_distance(i,k);
		double r_ij = TDOA_distance(i,j);

		double r_ik2 = pow(r_ik,2.0);
		double r_ij2 = pow(r_ij,2.0);

		double ix2 = pow(i[0],2.0);
		double iy2 = pow(i[1],2.0);
		double jx2 = pow(j[0],2.0);
		double jy2 = pow(j[1],2.0);
		double kx2 = pow(k[0],2.0);
		double ky2 = pow(k[1],2.0);

		double a1 = (r_ik*j[0])-(r_ik*i[0])-(r_ij*k[0])+(r_ij*i[0]);
		double a2 = (r_ik*j[1])-(r_ik*i[1])-(r_ij*k[1])+(r_ij*i[1]);

		double b1 = 	r_ij*(r_ik2+ix2-kx2+iy2-ky2)/2.0 -
						r_ik*(r_ij2+ix2-jx2+iy2-jy2)/2.0;

		A(var, 0) = a1;
		A(var, 1) = a2;
		b(var) = b1;
	}

	std::cout << A << std::endl << std::endl;
	std::cout << b << std::endl << std::endl;
	std::cout << "Solution:" << std::endl << A.colPivHouseholderQr().solve(b) <<std::endl;

}

int main() {
	std::vector<int*> antennas;
	CreateDummyAntennas(antennas);
	Mlat_Ruud(antennas);

}
