/*
 * Antenna.h
 *
 *  Created on: Nov 15, 2017
 *      Author: ruud
 */

#include <iostream>
#include <vector>

#ifndef ANTENNA_H_
#define ANTENNA_H_

class Antenna {
public:
	Antenna(int x, int y);
	virtual ~Antenna();
	void Print();
	int* getPos();

private:
	int pos[2];
};

#endif /* ANTENNA_H_ */
