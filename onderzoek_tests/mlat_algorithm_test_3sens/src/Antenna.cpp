/*
 * Antenna.cpp
 *
 *  Created on: Nov 15, 2017
 *      Author: ruud
 */

#include "Antenna.h"

Antenna::Antenna(int x, int y) {
	// TODO Auto-generated constructor stub
	this->pos[0] = x;
	this->pos[1] = y;
}

Antenna::~Antenna() {
	// TODO Auto-generated destructor stub
}

int* Antenna::getPos(){return this->pos;}

void Antenna::Print() {
	std::cout << "x: " << this->pos[0] << ", y: " << this->pos[1] << std::endl;
}